<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCustomerRequest;
use App\Models\Customer;
use App\Models\User;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    public function create(CreateCustomerRequest $request)
    {
        $create = Customer::createCustomer($request);
        if($create){
            return "Customer Created";
        }
        return "Error";
    }
}
