<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCustomerGroupRequest;
use App\Http\Requests\DeleteCustomerGroupRequest;
use App\Http\Requests\UpdateCustomerGroupRequest;
use App\Models\CustomerGroup;
use Illuminate\Http\Request;

class CustomerGroupController extends Controller
{
    public function create(CreateCustomerGroupRequest $request)
    {
        $create = CustomerGroup::createCustomerGroup($request);
        if($create){
            return "Customer Group Created";
        }
        return "Error";
    }

    public function update(UpdateCustomerGroupRequest $request)
    {
        $create = CustomerGroup::updateCustomerGroup($request);
        if($create){
            return "Customer Group Updated";
        }
        return "Error";
    }

    public function delete(DeleteCustomerGroupRequest $request)
    {
        $create = CustomerGroup::deleteCustomerGroup($request);
        if($create){
            return "Customer Group Deleted";
        }
        return "Error";
    }
}
