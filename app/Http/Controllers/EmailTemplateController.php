<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateEmailTemplateRequest;
use App\Models\EmailTemplate;
use Illuminate\Http\Request;

class EmailTemplateController extends Controller
{
    public function create(CreateEmailTemplateRequest $request)
    {
        $create = EmailTemplate::createEmailTemplate($request);
        if($create){
            return "Email Template Created";
        }
        return "Error";
    }
}
