<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateEmailTemplateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "customer_group_id" => "required|integer|exists:customer_groups",
            "subject" => "required|string",
            "subject_replacements" => "nullable|array",
            "body" => "required|string",
            "body_replacements" => "nullable|array",
        ];
    }
}
