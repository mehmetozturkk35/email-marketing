<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateCustomerGroupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => ['required', 'integer',
                Rule::exists('customer_groups', 'id')
            ],
            "name" => "required|string",
        ];
    }

    public function all($keys = null): array
    {
        $data = parent::all($keys);
        if (!empty($this->route('id'))) {
            $data['id'] = $this->route('id');
        }
        return $data;
    }
}
