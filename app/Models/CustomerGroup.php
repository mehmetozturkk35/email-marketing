<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomerGroup extends Model
{
    use HasFactory;

    public static function createCustomerGroup($request)
    {
        $customerGroup = new CustomerGroup();
        $customerGroup->name = $request->name;
        return $customerGroup->save();
    }

    public static function updateCustomerGroup($request)
    {
        $customerGroup = CustomerGroup::find($request->id);
        $customerGroup->name = $request->name;
        return $customerGroup->update();
    }

    public static function deleteCustomerGroup($request)
    {
        $customerGroupUsers = CustomerGroupUsers::where("customer_group_id", $request->id)->delete();
        if($customerGroupUsers){
            $customerGroup = CustomerGroup::find($request->id);
            return $customerGroup->delete();
        }
        return false;
    }
}
