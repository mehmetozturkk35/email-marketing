<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmailTemplate extends Model
{
    use HasFactory;

    public static function createEmailTemplate($request)
    {
        $emailTemplate = new EmailTemplate();
        $emailTemplate->customer_group_id = $request->customer_group_id;
        $emailTemplate->subject = $request->subject;
        $emailTemplate->subject_replacements = $request->subject_replacements;
        $emailTemplate->body = $request->body;
        $emailTemplate->body_replacements = $request->body_replacements;
        return $emailTemplate->save();
    }
}
