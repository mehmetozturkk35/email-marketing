<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmailSendingSchedule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_sending_schedule', function (Blueprint $table) {
            $table->id();
            $table->foreignId('email_template_id')->references('id')->on('email_templates');
            $table->dateTime("distribution_time");
            $table->integer("sent")->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_sending_schedule');
    }
}
