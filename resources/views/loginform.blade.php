@extends('layouts.login')

@section('loginform')

    <div class="kt-login__body">

        <div class="kt-login__logo">
            <a href="#">
                <!-- <img src="assets/media/" style="max-width: 250px;width: 100%;"> -->
            </a>
        </div>
        <div class="kt-login__signin">
            <div class="kt-login__head">
                <h3 class="kt-login__title">Sign in to Email Marketing</h3>
            </div>
            <div class="kt-login__form">
                <form class="kt-form">
                    <div class="form-group">
                        <input class="form-control" type="text" placeholder="Email" name="email" id="login_email" value="mehmetozturkk35@gmail.com" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <input class="form-control form-control-last" type="password" placeholder="Password" id="login_password" name="password" value="123456">
                    </div>
                    <div class="kt-login__extra">
                        <label class="kt-checkbox">
                            <input type="checkbox" name="remember"> Remember me
                            <span></span>
                        </label>
                        <a href="javascript:;" id="kt_login_forgot">Forget Password ?</a>
                    </div>
                    <div class="kt-login__actions">
                        <button type="button" id="kt_login_signin_submit" class="btn btn-brand btn-pill btn-elevate">Sign In</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="kt-login__signup">
            <div class="kt-login__head">
                <h3 class="kt-login__title">Sign Up</h3>
                <div class="kt-login__desc">Enter your details to create your account:</div>
            </div>
            <div class="kt-login__form">
                <form class="kt-form" id="singUp">
                    <div class="form-group">
                        <input class="form-control" type="text" placeholder="First Name" name="name" id="signup_name" required />
                    </div>
                    <div class="form-group">
                        <input class="form-control" type="text" placeholder="Last Name" name="last_name" id="signup_last_name" required />
                    </div>
                    <div class="form-group">
                        <input class="form-control" type="email" placeholder="Email" name="email" id="signup_email" autocomplete="off" required />
                    </div>
                    <div class="form-group">
                        <input class="form-control" type="password" placeholder="Password" name="password" id="signup_password" required />
                    </div>
                    <div class="form-group">
                        <input class="form-control form-control-last" type="password" placeholder="Confirm Password" name="password_confirmation" id="signup_password_confirmation" required />
                    </div>
                    <div class="kt-login__extra">
                        <label class="kt-checkbox">
                            <input type="checkbox" name="agree" required />
                            <a href="" data-toggle="modal" data-target="#kt_modal_2"> I Agree Terms And Conditions</a>
                            <span></span>
                        </label>
                    </div>
                    <div class="kt-login__actions">
                        <button id="kt_login_signup_submit" class="btn btn-brand btn-pill btn-elevate">Sign Up</button>
                        <button id="kt_login_signup_cancel" class="btn btn-outline-brand btn-pill">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="kt-login__forgot">
            <div class="kt-login__head">
                <h3 class="kt-login__title">Forgotten Password ?</h3>
                <div class="kt-login__desc">Enter your email to reset your password:</div>
            </div>
            <div class="kt-login__form">
                <form class="kt-form" id="singIn" action="">
                    <div class="form-group">
                        <input class="form-control" type="text" placeholder="Email" name="email" id="kt_email" autocomplete="off">
                    </div>
                    <div class="kt-login__actions">
                        <button id="kt_login_forgot_submit" class="btn btn-brand btn-pill btn-elevate">Request</button>
                        <button id="kt_login_forgot_cancel" class="btn btn-outline-brand btn-pill">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="kt_modal_2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Terms And Conditions</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body">
                    <p><b>PRIVACY and COOKIE POLICIES </b></p>                  <p><b>OUR PRIVACY POLICY</b></p>                  <p>When accessing our Website, Wezone will learn certain information about you during your visit. How we will handle this information and what we learn about you depends upon what you do when visiting our website.</p>                  <p>If you visit our site to read or download information on our pages, we only collect and store the following information about you:</p>                  <p>1.The name of the domain from which you access the Internet.</p>                 <p>2.The date and time you access our site</p>                 <p>3.The Internet address of the website you used to link directly to our site. If you choose to register on our site to use the services we provide, we collect and store only the following information about you:</p>                 <p>Email</p>                 <p>Name</p>                 <p>Surname</p>                 <p>Company Name</p>                 <p>Phone Number</p>                  <p>If you identify yourself by sending us an e-mail containing personal information, then the information collected will solely be used in response to your message.</p>                  <p>The information collected is for statistical purposes. Wezone may use software programs to create summary statistics, which can be used for such purposes as assessing the number of visitors to the different sections of our site, what information is of most and least interest, determining technical design specifications, and identifying system performance or problem areas.</p>                  <p> For site security purposes and to ensure that this service remains available to all users, Wezone uses software programs to monitor network traffic to identify unauthorized attempts to upload or change information, or otherwise cause damage.</p>                  <p>Wezone will not obtain personally-identifying information about you when you visit our site, unless you choose to provide such information to us by registering to our site, nor will such information be sold or otherwise transferred to unaffiliated third parties without the approval of the user at the time of collection.</p>                  <p>At any time, you may request if and how your data is processed. You may also request to receive information with regards to third parties if your personal data is transferred to.  Similarly, at any time, you may request to delete or amend all of your personal data in our database. For all your enquiries and requests in regards to personal data protection and our privacy policy, please contact our data supervisor at info@wezone.io.</p>                  <p><b>OUR COOKIE POLICY</b></p>                  <p>This policy will let you understand what type of cookies we use, what type of information we collect and how that information is used. By using our website you agree that we can use cookies in accordance with this policy.</p>                  <p><b>What are the Cookies and Why Do We Use Them?</b></p>                  <p>Cookies are small text files stored on hard drive of your device or computer, on the browser or network server.</p>                  <p>We use cookies in order to;</p>                  <p>Distinguish you from other users and provide a personalised browsing experience</p>                 <p>Improve the services offered to you by increasing the functionality and performance</p>                 <p>Provide legal and commercial security for you and our company</p>                  <p><b>What type of cookies do we use?</b></p>                  <p><b>Functionality Cookies</b> improve the functionality of the website by keeping your preferences. These cookies store information about your previous choices (language, location etc.)-in simpler words they remember you- and allow us to provide more personal user experience.</p>                 <p><b>Targeting Cookies</b> are used to deliver you personalised content. They can be used for targeted advertising or measuring a campaign. This information is used to make our website and the advertising displayed on it more relevant to your preferences. We may use these cookies to remember the sites you visit, in order to identify which channels are more efficient and allow us to reward partners or external websites.</p>                 <p><b>Session (Transient) Cookies </b> allow users to be remembered within a website so any page changes users do is recognised. Session cookies are stored in temporary memory and are not retained after the browser is closed and they do not collect any data from the user’s devices.</p>                 <p><b>Authentication Cookies </b> store session ID information in the users browser after a successful login.</p>                 <p><b> How Do You Disable Cookies?</b></p>                  <p>You can disable cookies by activating the setting on your browser that allows you to refuse the setting of all or some cookies. If you block our use of cookies, you may not access certain features/pages or areas of our website.</p>                  <p> This policy might be updated due to the changes in legislation and technology in the future. Therefore, you should visit this page from time to time to ensure you are familiar with any changes.</p>            </div>
            </div>
        </div>
    </div>
@endsection
