<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'App\Http\Controllers\IndexController@dashboard')->name('dashboard')->middleware('auth');


Route::get('/login', 'App\Http\Controllers\UserController@login')->name('login');
Route::get('/logout', 'App\Http\Controllers\UserController@logout')->name('logout');
Route::post('/sendLogin', 'App\Http\Controllers\UserController@login')->name('sendLogin');
Route::get('/dashboard', 'App\Http\Controllers\IndexController@dashboard')->name('dashboard')->middleware('auth');
